package com.example.sportshop;

import java.sql.SQLException;

public class Connect {
    protected ConnectDB db;

    protected int counterId;

    Connect() throws SQLException {
        db = new ConnectDB();
        initTable();
    }


    void initTable() {
        try {
            db.execute("CREATE TABLE IF NOT EXISTS category (\n" +
                    "  id INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(45) NOT NULL,\n" +
                    "  PRIMARY KEY (id))\n" +
                    "ENGINE = InnoDB\n" +
                    "AUTO_INCREMENT = 5\n" +
                    "DEFAULT CHARACTER SET = utf8;");
            db.execute("CREATE TABLE IF NOT EXISTS role (\n" +
                    "                    id INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "                    name VARCHAR(45) NOT NULL,\n" +
                    "                    PRIMARY KEY (id))\n" +
                    "            ENGINE = InnoDB\n" +
                    "            AUTO_INCREMENT = 4\n" +
                    "            DEFAULT CHARACTER SET = utf8;");


            db.execute("CREATE TABLE IF NOT EXISTS employee ( id INT(11) NOT NULL AUTO_INCREMENT, fio VARCHAR(100) NOT NULL, age INT(11) NOT NULL, adress VARCHAR(100) NOT NULL, phone VARCHAR(45) NOT NULL, login VARCHAR(111) NOT NULL, password VARCHAR(45) NOT NULL, idRole INT(11) NOT NULL, last_login DATE NULL DEFAULT NULL, PRIMARY KEY (id), CONSTRAINT idRole FOREIGN KEY (idRole) REFERENCES role (id) ON DELETE NO ACTION ON UPDATE NO ACTION) ENGINE = InnoDB AUTO_INCREMENT = 8 DEFAULT CHARACTER SET = utf8;");


            db.execute("CREATE TABLE IF NOT EXISTS status (\n" +
                    "  id INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(45) NOT NULL,\n" +
                    "  PRIMARY KEY (id))\n" +
                    "ENGINE = InnoDB\n" +
                    "AUTO_INCREMENT = 6\n" +
                    "DEFAULT CHARACTER SET = utf8;");

            db.execute("CREATE TABLE IF NOT EXISTS orderr (\n" +
                    "  id INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "  price INT(11) NOT NULL,\n" +
                    "  data DATE NOT NULL,\n" +
                    "  idStatus INT(11) NOT NULL,\n" +
                    "  nameProduct VARCHAR(45) NULL DEFAULT NULL,\n" +
                    "  fioClient VARCHAR(45) NULL DEFAULT NULL,\n" +
                    "  adress VARCHAR(45) NULL DEFAULT NULL,\n" +
                    "  PRIMARY KEY (id),\n" +
                    "  CONSTRAINT idStatus\n" +
                    "    FOREIGN KEY (idStatus)\n" +
                    "    REFERENCES status (id)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION)\n" +
                    "ENGINE = InnoDB\n" +
                    "AUTO_INCREMENT = 12\n" +
                    "DEFAULT CHARACTER SET = utf8;");

            db.execute("CREATE TABLE IF NOT EXISTS product (\n" +
                    "  id INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(70) NOT NULL,\n" +
                    "  price INT(11) NOT NULL,\n" +
                    "  discription VARCHAR(200) NOT NULL,\n" +
                    "  photo VARCHAR(45) NOT NULL,\n" +
                    "  idCategory INT(11) NOT NULL,\n" +
                    "  PRIMARY KEY (id),\n" +
                    "  CONSTRAINT idCategory\n" +
                    "    FOREIGN KEY (idCategory)\n" +
                    "    REFERENCES category (id)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION)\n" +
                    "ENGINE = InnoDB\n" +
                    "AUTO_INCREMENT = 32\n" +
                    "DEFAULT CHARACTER SET = utf8;");

            db.execute("CREATE TABLE IF NOT EXISTS order_product (\n" +
                    "  id INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "  order_id INT(11) NOT NULL,\n" +
                    "  product_id INT(11) NOT NULL,\n" +
                    "  PRIMARY KEY (id),\n" +
                    "  CONSTRAINT order_id\n" +
                    "    FOREIGN KEY (order_id)\n" +
                    "    REFERENCES orderr (id)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION,\n" +
                    "  CONSTRAINT product_id\n" +
                    "    FOREIGN KEY (product_id)\n" +
                    "    REFERENCES product (id)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION)\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");

            db.execute("INSERT IGNORE INTO category (id, name) VALUES (1, 'Комплектующие ПК'), (2, 'Аксессуары'), (3, 'Мониторы');");
            db.execute("INSERT IGNORE INTO employee (id, fio, age, adress, phone, login, password, idRole, last_login) VALUES (5, 'Cvetov Alexander Alexandrovich', 19, 'Nizhniy Novgorod', '89059447634', '1', '1', 1, '2023-01-22'), (6, 'Fedorov Ivan Ivanovich', 22, 'Moscow', '89547334658', '2', '2', 2, '2023-01-22');");
            db.execute("INSERT IGNORE INTO orderr (id, price, data, idStatus, nameProduct, fioClient, adress) VALUES\n" +
                    "(3, 4874, '2022-11-21', 1, 'Wi-Fi роутер HUAWEI WiFi AX3 ', 'Cvetov Alexander Alexandrovich', 'Nizhniy Novgorod'),\n" +
                    "(4, 3899, '2022-11-21', 1, 'Computer power supply XPG', 'Cvetov Alexander Alexandrovich', 'Nizhniy Novgorod'),\n" +
                    "(11, 190999, '2022-11-22', 1, 'Iphone', 'Kira D. D.', 'Kiev');\n");
            db.execute("INSERT IGNORE INTO product (id, name, price, discription, photo, idCategory) VALUES\n" +
                    "(1, 'Computer power supply XPG', 3000, 'Блок питания XPG Corereactor 750G – модель, сертифицированная по стандарту 80 Plus на уровне Gold. Это указывает на высокую эффективность прибора, особенно при высоких нагрузках.', 'image11', 1),\n" +
                    "(2, 'Screen HUAWEI MateView SE SSN-24', 3899, 'Экран монитора Huawei MateView SE SSN-24 создан по технологии IPS. Его диагональ — 23,8 дюйма, разрешение — 1920 х 1080 пикселей, частота обновления — 75 Гц. Максимальные углы обзора по вертикали и го', 'image12', 3),\n" +
                    "(3, 'Kingston Internal SSD Drive', 2000, 'KINGSTON A400 SA400M8/120G – доступный накопитель в форм-факторе М.2. Модель подойдет в первую очередь для апгрейда старых ПК и ноутбуков. Работа с интерфейсом SATA III позволяет добиться скорости чте', 'image13', 1),\n" +
                    "(4, 'Wi-Fi роутер HUAWEI WiFi AX3 ', 4874, 'Роутер Huawei AX3 поддерживает стандарт беспроводной связи IEEE 802.11ax, известный также как Wi-Fi 6 Plus, Благодаря этому он способен передавать данные со скоростью до 2402 Мбит/с.', 'image14', 2),\n" +
                    "(5, 'Video card MSI GeForce RTX 3060 ', 4999, 'Видеокарта MSI GeForce RTX 3060 Ventus 3X 12G OC подходит для создания высокопроизводительного компьютера для игр. Изготовитель интегрированного в нее графического процессора — компания Nvidia. Тип па', 'image21', 1),\n" +
                    "(6, 'HIKVISION Flash Drive', 6999, 'USB-накопитель HIKVISION расширяет емкость хранилища для устройств и обеспечивает быстрое резервное копирование и хранение файлов. Предоставляет устройства Android с интерфейсом USB с быстрой, стабиль', 'image22', 2),\n" +
                    "(7, 'Iphone', 190999, 'Pro Max', 'phon', 2);");
            db.execute("INSERT IGNORE INTO role (id, name) VALUES\n" +
                    "(1, 'Клиент'),\n" +
                    "(2, 'Продавец');");
            db.execute("INSERT IGNORE INTO status (id, name) VALUES\n" +
                    "(1, 'В обработке '),\n" +
                    "(2, 'В доставке'),\n" +
                    "(3, 'Готов');");



        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
