package com.example.sportshop;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

public class Delete implements Initializable {

    @FXML
    public TextField name;
    @FXML
    public TextField cost;
    @FXML
    public TextArea dop;
    @FXML
    public TextField foto;
    @FXML
    public ComboBox type;
    @FXML
    public Button newObyvlenie ;
    @FXML
    public Button btnBack ;
    @FXML
    private Label infoLabel;

    ConnectDB db = new ConnectDB();
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ArrayList<String> tovarType= null;
        try {
            tovarType = db.getTovar();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }
        type.getItems().addAll(tovarType);

    }



    public void onNewObyavleniy(ActionEvent actionEvent) throws SQLException, ClassNotFoundException, ParseException {

        String typeText = (String) type.getValue();

        if (type.getValue() != null){
            db.delete(typeText);
            infoLabel.setText("товар успешно удален");
        }else
            infoLabel.setText("товар не выбран");

    }
    public void btnbac() throws IOException {
        Parent wLogin = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("adminScreen.fxml")));
        Stage window = (Stage) btnBack.getScene().getWindow();
        window.setScene(new Scene(wLogin, 960, 540));
        window.show();
    }

}