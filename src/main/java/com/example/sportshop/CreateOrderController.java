package com.example.sportshop;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

public class CreateOrderController implements Initializable {
    @FXML
    private TextField adressLabel;

    @FXML
    private Button btnBack;
    @FXML
    private Button btnBackA;


    @FXML
    private ComboBox combobox;

    @FXML
    private TextField fioLabel;

    @FXML
    private Label infoLabel;

    @FXML
    private Label priceLabel;


ConnectDB db = new ConnectDB();

    public void initialize(URL url, ResourceBundle resourceBundle) {

        ArrayList<String> tovarType= null;
        try {
            tovarType = db.getTovarProduct();
            priceLabel.setText("0");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }
        combobox.getItems().addAll(tovarType);

    }

    public void createOrder(ActionEvent actionEvent) throws SQLException, ClassNotFoundException, ParseException {


        if (!fioLabel.getText().equals("") && !adressLabel.getText().equals("") && combobox.getValue() != null){
            priceLabel.setText(getPrice("Select price from mydb.product where name = '"+combobox.getValue()+"';"));
            db.sendOrder(fioLabel.getText(), adressLabel.getText(), (String) combobox.getValue(),priceLabel.getText());
            infoLabel.setText("Заказ успешно оформлен");
        }else
            infoLabel.setText("Не все поля заполнены");

    }

    private String getPrice(String select) throws SQLException {
        String price = "0";
        ResultSet result = db.getProduct(select);
        while (result.next()) {
           price = String.valueOf(result.getInt(1));
        }
        return price;
    }
    public void btnbac() throws IOException {
        Parent wLogin = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("menuScreen.fxml")));
        Stage window = (Stage) btnBack.getScene().getWindow();
        window.setScene(new Scene(wLogin, 960, 540));
        window.show();
    }
    public void btnbacA() throws IOException {
        Parent wLogin = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("menuAdmin.fxml")));
        Stage window = (Stage) btnBackA.getScene().getWindow();
        window.setScene(new Scene(wLogin, 960, 540));
        window.show();
    }


}
